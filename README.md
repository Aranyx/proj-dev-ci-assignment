# Welcome to es6test 👋
> es6-twilio-chatbot

### This is a haunted house simulation chat bot app developed for the Halloween holiday. This gives a real life like experience in the simulation.

### 🏠 [Homepage](https://Aranyx@bitbucket.org/Aranyx/proj-dev-ci-assignment.git)

### ✨ [Demo](https://aravind-ci-test-app.herokuapp.com/)

## Install Node Modules

```sh
npm install
```

## To Run the application

```sh
npm run start
```

## To Run tests

```sh
npm run test
```
# Heroku Deployment

## Install the Heroku CLI

### Download and install the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli).

If you haven't already, log in to your Heroku account and follow the prompts to create a new SSH public key.

```sh
$ heroku login
```

Make some changes to the code you just cloned and deploy them to Heroku using Git.

```sh
$ git add .
$ git commit -am "make it better"
$ git push heroku master
```

## Author

👤 **Aravind Kumar**

## License Info
The MIT License (MIT)- grants full permission to use my project with or without any modifications

* Website: https://aravind-ci-test-app.herokuapp.com/
